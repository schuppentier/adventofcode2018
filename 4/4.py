import re

diary = []
guards = {}

with open("data.txt") as data:
    pattern = re.compile("^\[(?P<year>\d*)\-(?P<month>\d*)\-(?P<day>\d*)\s(?P<hour>\d*)\:(?P<minute>\d*)\]\s(?P<data>.*)$", re.VERBOSE)
    for line in sorted(data.read().split("\n")):
        match = pattern.match(line)

        year = int(match.group('year'))
        month = int(match.group('month'))
        day = int(match.group('day'))
        hour = int(match.group('hour'))
        minute = int(match.group('minute'))
        data = match.group('data').split(" ")

        if data[0] == "Guard":
            guard_id = data[1]
            continue

        if data[0] == "falls":
            fallasleepminute = minute
            continue

        if data[0] == "wakes":
            date = "{}-{}".format(month, day)
            for iminute in range(fallasleepminute, minute):
                guards.setdefault(guard_id, {}).setdefault(date, {})[iminute] = "#"

for guard_id, data in guards.items():
    minutes_asleep = 0
    minutedata = {}
    for dayname, day in data.items():
        if "-" not in dayname:
            continue
        minutes_asleep += len(day)
        for minute in day.keys():
            minutedata.setdefault(minute, 0)
            minutedata[minute] += 1
    guards[guard_id]['minutes_asleep'] = minutes_asleep
    guards[guard_id]['minutedata'] = minutedata

sleepminutes_max = 0
for guard_id, data in guards.items():
    if data['minutes_asleep'] > sleepminutes_max:
        sleepminutes_max = data['minutes_asleep']
        sleepminutes_max_guard_id = guard_id

sleepminute_count_max = 0
for minute, count in guards[sleepminutes_max_guard_id]['minutedata'].items():
    if count > sleepminute_count_max:
        sleepminute_count_max = count
        sleepminute_count_max_minute_id = minute

print("Faulster Guard: {}, schläft {} Minuten, am meisten zu Minute {}".format(sleepminutes_max_guard_id, sleepminutes_max, sleepminute_count_max_minute_id))
print("Ergebnis 1: {}".format(int(sleepminutes_max_guard_id[1:])*sleepminute_count_max_minute_id))

bestminute_count = 0
for guard_id, guard_data in guards.items():
    for minute, minutedata in guard_data['minutedata'].items():
        if minutedata > bestminute_count:
            bestminute_count = minutedata
            bestminute = minute
            bestguard = guard_id

print("Ergebnis 2: {}".format(int(bestguard[1:])*bestminute))