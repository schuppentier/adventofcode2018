from string import ascii_lowercase

polymerInput = list(open("data.txt").read())

def react(polymer):
    polymer = polymer.copy()
    changed = True
    while changed:
        changed = False
        for idx, char in enumerate(polymer):
            if not idx < len(polymer) - 1:
                break
            if polymer[idx].lower() == polymer[idx + 1].lower() and polymer[idx] != polymer[idx + 1]:
                del polymer[idx + 1]
                del polymer[idx]
                changed = True
    return polymer

result1 = react(polymerInput)

print("Ergebnis 1: {}".format(len(result1)))


def remove_unit(polymer, unitToRemove):
    polymer = polymer.copy()
    idxToDelete = []
    for idx, char in enumerate(polymer):
        if polymer[idx].lower() == unitToRemove:
            idxToDelete.append(idx)
    for idx in reversed(idxToDelete):
        del polymer[idx]
    return polymer

for charToRemove in ascii_lowercase:
    cleaned_polymer = remove_unit(polymerInput, charToRemove)
    cleaned_reacted_polymer = react(cleaned_polymer)
    print("Ergebnisgröße, wenn {} entfernt wird: {}".format(charToRemove, len(cleaned_reacted_polymer)))