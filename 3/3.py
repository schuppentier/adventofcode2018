import re

cuts = {}

with open("data.txt") as data:
    pattern = re.compile("^(?P<id>.*) @ (?P<x>.*),(?P<y>.*): (?P<dx>.*)x(?P<dy>.*)$", re.VERBOSE)
    for line in data.read().split("\n"):
        match = pattern.match(line)

        id = match.group("id").strip()
        x = int(match.group("x"))
        y = int(match.group("y"))
        dx = int(match.group("dx"))
        dy = int(match.group("dy"))
        cuts[id] = {'x': x, 'y': y, 'dx': dx, 'dy': dy}

fabric = [[0 for x in range(1000)] for y in range(1000)]

maxwidth = 0
maxheight = 0
for cutid, cutdata in cuts.items():
    for x in range(cutdata['x'], cutdata['x'] + cutdata['dx']):
        for y in range(cutdata['y'], cutdata['y'] + cutdata['dy']):
            fabric[x][y] += 1

multis = 0
for x in range(1000):
    for y in range(1000):
        if fabric[x][y] > 1:
            multis += 1

print("Mehrfachfelder: {}".format(multis))

for cutid, cutdata in cuts.items():
    is_solid = True
    for x in range(cutdata['x'], cutdata['x'] + cutdata['dx']):
        for y in range(cutdata['y'], cutdata['y'] + cutdata['dy']):
            if fabric[x][y] > 1:
                is_solid = False
    if is_solid:
        print("Claim {} ist vollständig!".format(cutid))
