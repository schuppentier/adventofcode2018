def count(id):
    seen = {}
    twice = False
    thrice = False

    for x in id:
        if x not in seen:
            seen[x] = 1
        else:
            seen[x] += 1
    for char in seen:
        if seen[char] == 2:
            twice = True
        if seen[char] == 3:
            thrice = True
    return [twice, thrice]

data = open("data.txt").read().split("\n")

serialnumbersWithTwo = 0
serialnumbersWithThree = 0
for serialnumber in data:
    check = count(serialnumber)
    if check[0]:
        serialnumbersWithTwo += 1
    if check[1]:
        serialnumbersWithThree += 1

print("Doppel: {}, Tripel: {}, Produkt: {}".format(serialnumbersWithTwo, serialnumbersWithThree, serialnumbersWithTwo * serialnumbersWithThree))

def compareSerialnumbers(one, two):
    diffcount = 0
    for x, y in zip(one, two):
        if x != y:
            diffcount += 1
    if diffcount == 1:
        return True
    return False

def removeDiffChar(one, two):
    result = ''
    for x, y in zip(one, two):
        if x == y:
            result += x
    return result

for index, serialnumber in enumerate(data):
    for serialnumberToCompare in data[index+1:]:
        if compareSerialnumbers(serialnumber, serialnumberToCompare):
            print("Ergebnis: {}".format(removeDiffChar(serialnumber, serialnumberToCompare)))